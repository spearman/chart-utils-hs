-- | Chart utilities.

{-# OPTIONS_GHC -Wall #-}
{-# OPTIONS_GHC -fno-warn-name-shadowing #-}
{-# LANGUAGE NoImplicitPrelude #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE TypeApplications #-}

module Utils.Chart (
  layout_axes_hidden,
  plot_bars,
  plot_bars_sequence,
  plot_histogram_normalized,
  plot_line_fill,
  plot_lines_sequences,
  plot_lines_square,
  plot_points,
  style_bars_solid
) where

import BasicPrelude hiding (Vector)

import qualified Data.Colour.Names                         as Colour
import qualified Data.Vector.Storable                      as Vector
import qualified Graphics.Rendering.Chart.Easy             as Chart
import qualified Graphics.Rendering.Chart.Backend.Cairo    as Chart.Cairo
import qualified System.FilePath                           as FilePath

import Data.Bifunctor                (bimap)
import Data.Colour                   (Colour)
import Data.Vector.Storable          (Vector)
import Foreign.Storable.Tuple        ()
import Graphics.Rendering.Chart.Easy (def, (.=))

-- | Plot sequences of scalar values as line graphs.
--
-- Each list of sequences is drawn in a different color.
plot_lines_sequences :: FilePath
  -> Maybe (Chart.EC (Chart.Layout Double Double) ()) -> [[Vector Double]]
  -> IO ()
plot_lines_sequences filepath layout sequences = to_file filepath $ do
  fromMaybe (return def) layout
  forM_ sequences $ \list -> Chart.plot $ Chart.line "" $ map values list
  where values sequence = zip [0.0::Double ..] $ Vector.toList sequence

-- | Plot lines with a square aspect ratio.
--
-- Each list of lines is drawn in a different color.
plot_lines_square :: FilePath
  -> Maybe (Chart.EC (Chart.Layout Double Double) ())
  -> [[Vector (Double, Double)]]
  -> IO ()
plot_lines_square filepath layout lines = to_file filepath $ do
  fromMaybe (return def) layout
  Chart.layout_x_axis . Chart.laxis_generate .= Chart.scaledAxis def x_bounds
  Chart.layout_y_axis . Chart.laxis_generate .= Chart.scaledAxis def y_bounds
  forM_ lines $ \list -> Chart.plot $ Chart.line "" $ map Vector.toList list
  where
    x_bounds = (min x_min y_min, max x_max y_max)
    y_bounds = (min x_min y_min, max x_max y_max)
    x_min    = minimum $ map (Vector.minimum . Vector.map fst) $ concat lines
    x_max    = maximum $ map (Vector.maximum . Vector.map fst) $ concat lines
    y_min    = minimum $ map (Vector.minimum . Vector.map snd) $ concat lines
    y_max    = maximum $ map (Vector.maximum . Vector.map snd) $ concat lines

-- | Plot line with fill color between values
plot_line_fill :: FilePath
  -> Maybe (Chart.EC (Chart.Layout Double Double) ())
  -> Vector (Double, Double)
  -> IO ()
plot_line_fill filepath layout line = to_file filepath $ do
  fromMaybe (return def) layout
  Chart.plot $ Chart.liftEC $ do
    Chart.plot_fillbetween_style
      .= (Chart.FillStyleSolid $ Chart.opaque Colour.blue :: Chart.FillStyle)
    Chart.plot_fillbetween_values .= Vector.toList
      ( Vector.map (\(x, y) -> (x, (0.0, y))) line
        :: Vector (Double, (Double, Double)))

-- | Plot a sequence of values as a bar chart
plot_bars_sequence :: FilePath
  -> Maybe (Chart.EC (Chart.Layout Double Double) ()) -> Vector Double
  -> IO ()
plot_bars_sequence filepath layout sequence
  = plot_bars filepath layout bars where
    bars = Vector.zipWith (,)
      (Vector.generate (Vector.length sequence) fromIntegral) sequence

-- | Plot bar chart
plot_bars :: FilePath
  -> Maybe (Chart.EC (Chart.Layout Double Double) ()) -> Vector (Double, Double)
  -> IO ()
plot_bars filepath layout bars = to_file filepath $ do
  fromMaybe (return def) layout
  Chart.plot $ map Chart.plotBars $ Chart.liftEC $ do
    style_bars_solid Colour.blue
    Chart.plot_bars_values .= values
    where values = map (bimap id (:[])) $ Vector.toList bars

-- | Plot a normalized histogram with the given number of bins
plot_histogram_normalized :: FilePath
  -> Maybe (Chart.EC (Chart.Layout Double Double) ()) -> Vector Double -> Int
  -> IO ()
plot_histogram_normalized filepath layout samples bins = to_file filepath $ do
  fromMaybe (return def) layout
  Chart.plot $ return $ Chart.histToPlot $ Chart.defaultNormedPlotHist {
    Chart._plot_hist_bins   = bins,
    Chart._plot_hist_values = Vector.toList samples
  }

-- TODO: multiple sets of points
-- | Scatter plot
plot_points :: FilePath
  -> Maybe (Chart.EC (Chart.Layout Double Double) ())
  -> [Vector (Double, Double)]
  -> IO ()
plot_points filepath layout points = to_file filepath $ do
  fromMaybe (return def) layout
  Chart.layout_x_axis . Chart.laxis_generate .= Chart.scaledAxis def x_bounds
  Chart.layout_y_axis . Chart.laxis_generate .= Chart.scaledAxis def y_bounds
  forM_ points $ \list -> Chart.plot $ Chart.points "" $ Vector.toList list
  where
    x_bounds = (min x_min y_min, max x_max y_max)
    y_bounds = (min x_min y_min, max x_max y_max)
    x_min    = minimum $ map (Vector.minimum . Vector.map fst) points
    x_max    = maximum $ map (Vector.maximum . Vector.map fst) points
    y_min    = minimum $ map (Vector.minimum . Vector.map snd) points
    y_max    = maximum $ map (Vector.maximum . Vector.map snd) points

-- | Sets bar plot to right alignment, zero gap, minwidth 1, and solid color
-- with no outline. This is the default style used for 'plot_bars' and
-- 'plot_bars_sequence'
style_bars_solid
  :: Colour Double -> Chart.EC (Chart.PlotBars Double Double) ()
style_bars_solid color = do
  Chart.plot_bars_alignment   .= Chart.BarsRight
  Chart.plot_bars_spacing     .= Chart.BarsFixGap 0.0 1.0
  Chart.plot_bars_item_styles .= repeat
    (Chart.FillStyleSolid $ Chart.opaque color, Nothing)

-- | Sets chart axes to be invisible
layout_axes_hidden :: Chart.EC (Chart.Layout x y) ()
layout_axes_hidden = do
  -- NOTE: setting axis label visibility to False will cause bars to
  -- disappear, so we set the axis style colors to white to make them
  -- invisible
  Chart.layout_left_axis_visibility
    .= Chart.AxisVisibility False False True
  Chart.layout_bottom_axis_visibility
    .= Chart.AxisVisibility False False True

  Chart.layout_y_axis . Chart.laxis_style . Chart.axis_label_style
    . Chart.font_color .= Chart.opaque Colour.white
  Chart.layout_y_axis . Chart.laxis_style . Chart.axis_line_style
    . Chart.line_color .= Chart.opaque Colour.white
  Chart.layout_y_axis . Chart.laxis_style . Chart.axis_grid_style
    . Chart.line_color .= Chart.opaque Colour.white

  Chart.layout_x_axis . Chart.laxis_style . Chart.axis_label_style
    . Chart.font_color .= Chart.opaque Colour.white
  Chart.layout_x_axis . Chart.laxis_style . Chart.axis_line_style
    . Chart.line_color .= Chart.opaque Colour.white
  Chart.layout_x_axis . Chart.laxis_style . Chart.axis_grid_style
    . Chart.line_color .= Chart.opaque Colour.white

--
--  private
--

to_file :: FilePath -> Chart.EC (Chart.Layout Double Double) () -> IO ()
to_file filepath
  = Chart.Cairo.toFile (def { Chart.Cairo._fo_format = file_format }) filepath
  where
    file_format = case FilePath.takeExtension filepath of
      ".svg" -> Chart.Cairo.SVG
      ".SVG" -> Chart.Cairo.SVG
      ".png" -> Chart.Cairo.PNG
      ".PNG" -> Chart.Cairo.PNG
      ".ps"  -> Chart.Cairo.PS
      ".PS"  -> Chart.Cairo.PS
      ".pdf" -> Chart.Cairo.PDF
      ".PDF" -> Chart.Cairo.PDF
      _ -> error "allowed extensions: .svg .png .ps .pdf"
