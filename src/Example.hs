{-# OPTIONS_GHC -Wall #-}
{-# OPTIONS_GHC -fno-warn-name-shadowing #-}
{-# LANGUAGE NoImplicitPrelude #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE TypeApplications #-}

module Main where

import ClassyPrelude hiding (Vector, (<>))

import qualified Data.Vector.Storable as Vector
import qualified System.Random        as Random

import qualified Utils.Chart as Chart

main :: IO ()
main = do
  putStrLn "chart-utils example main..."
  Chart.plot_lines_sequences "sequences.svg" (Just Chart.layout_axes_hidden)
    [ [Vector.generate 10 $ (*2.0)       . fromIntegral],
      [Vector.generate 10 $ (*(1.0/3.0)) . fromIntegral]]
  Chart.plot_lines_sequences "sequences.png" (Just Chart.layout_axes_hidden)
    [ [Vector.generate 10 $ (*2.0)       . fromIntegral],
      [Vector.generate 10 $ (*(1.0/3.0)) . fromIntegral]]
  Chart.plot_lines_square "lines.svg" (Just Chart.layout_axes_hidden)
    [ [Vector.generate 10 $ (\i -> (i, i * 2.0))       . fromIntegral],
      [Vector.generate 10 $ (\i -> (i, i * (1.0/3.0))) . fromIntegral]]
  Chart.plot_lines_square "lines.png" (Just Chart.layout_axes_hidden)
    [ [Vector.generate 10 $ (\i -> (i, i * 2.0))       . fromIntegral],
      [Vector.generate 10 $ (\i -> (i, i * (1.0/3.0))) . fromIntegral]]
  Chart.plot_bars_sequence "a.svg" (Just Chart.layout_axes_hidden)
    $ Vector.generate 64 $ \i -> let x = fromIntegral i :: Double in
      sin $ (x / 64.0) * 2.0 * pi
  v <- Vector.generateM 100000 $ const Random.randomIO
  --putStrLn $ tshow v
  Chart.plot_histogram_normalized "hist.png" Nothing v 100

  Chart.plot_line_fill "line-fill.png" (Just Chart.layout_axes_hidden)
    $ Vector.generate 10 (\x -> (fromIntegral x, 2.0 * fromIntegral x))

  let wave freq t = sin $ (freq * 2 * pi * t) / 48000.0
  Chart.plot_lines_sequences "wave.png" (Just Chart.layout_axes_hidden)
    [ [Vector.generate 48000 $ (wave 10.0) . fromIntegral] ]

  xs <- Vector.generateM 200 $ const Random.randomIO
  ys <- Vector.generateM 200 $ const Random.randomIO
  let v1  = Vector.zipWith (,) xs ys
  xs <- Vector.generateM 200 $ const Random.randomIO
  ys <- Vector.generateM 200 $ const Random.randomIO
  let v2  = Vector.zipWith (,) xs ys
  --putStrLn $ tshow v
  Chart.plot_points "points.png" Nothing [v1, v2]

  putStrLn "...chart-utils example main"
