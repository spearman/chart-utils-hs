#!/bin/sh

set -x

cabal haddock && firefox dist-newstyle/build/x86_64-linux/ghc-*/chart-utils-*/doc/html/chart-utils/index.html

exit 0
