{ nixpkgs ? import <nixpkgs> {}, compiler ? "default", doBenchmark ? false }:

let

  inherit (nixpkgs) pkgs;

  f = { mkDerivation, base, basic-prelude, Chart, Chart-cairo, classy-prelude
      , colour, filepath, random, stdenv, storable-tuple, vector
      }:
      mkDerivation {
        pname = "chart-utils";
        version = "0.1.0.0";
        src = ./.;
        isLibrary = true;
        isExecutable = true;
        libraryHaskellDepends = [
          base basic-prelude Chart Chart-cairo colour filepath random
          storable-tuple vector
        ] ++ mydevtools;
        executableHaskellDepends = [ base classy-prelude vector ] ++ mydevtools;
        description = "Chart utilities";
        license = stdenv.lib.licenses.asl20;
      };

  haskellPackages = if compiler == "default"
                       then pkgs.haskellPackages
                       else pkgs.haskell.packages.${compiler};

  variant = if doBenchmark then pkgs.haskell.lib.doBenchmark else pkgs.lib.id;

  drv = variant (haskellPackages.callPackage f {});

  mydevtools = [
    pkgs.feh
    haskellPackages.cabal-install
    haskellPackages.ghc
    haskellPackages.ghcide
    # ghc-prof broken
    #haskellPackages.profiterole
    #haskellPackages.profiteur
  ];
in

  if pkgs.lib.inNixShell then drv.env else drv
